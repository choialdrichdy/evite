from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from evite.config import db_file

engine = create_engine(f'sqlite:///{db_file}')
session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)

def get_session():
    return Session()

class SessionManager:
    """
    Create a session for every request and close it when the request ends.
    """

    def __init__(self, Session):
        self.session = Session

    def process_resource(self, req, resp, resource, params):
        if req.method == 'OPTIONS':
            return

        req.context['session'] = self.session()

    def process_response(self, req, resp, resource, req_succeeded):
        if req.method == 'OPTIONS':
            return

        if req.context.get('session'):
            if not req_succeeded:
                req.context['session'].rollback()
            req.context['session'].close()