import os

db_file = os.getenv('db_file', 'evite.db')
smtp_port = os.getenv('smtp_port', 25)
smtp_host = os.getenv('smtp_host', 'localhost')
sender = os.getenv('sender', 'notifications@evite.com')
receiver = os.getenv('receiver', 'admin@evite.com')