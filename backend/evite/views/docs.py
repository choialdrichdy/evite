import inspect
from apispec import APISpec, BasePlugin, yaml_utils
from apispec.ext.marshmallow import MarshmallowPlugin

from evite.models.event import EventSchema, EventAttendeeSchema


uri_blocklist = [
    '/',
    '/oauth2-redirect.html',
    '/docs'
]

def get_all_nodes(api, use_blocklist=False):
    nodes_list = []

    def get_children(node):
        if all([node.uri_template, node.resource]):
            nodes_list.append(node)
        if node.children:
            for child_node in node.children:
                get_children(child_node)

    for node in api._router._roots:
        get_children(node)
    if use_blocklist:
        nodes_list = [n for n in nodes_list if n.uri_template not in uri_blocklist]
    return nodes_list

class FalconPlugin(BasePlugin):

    def path_helper(self, operations, *, node, **kwargs):
        for method, func in node.method_map.items():
            if hasattr(func, '__self__'):
                method = method.lower()
                operations[method] = yaml_utils.load_yaml_from_docstring(func.__doc__)
                parameters = inspect.signature(func).parameters
                if len(parameters) > 2:
                    required_parameters = []
                    for name in list(parameters)[2:]:
                        parameter = {
                            'in': 'path',
                            'name': name,
                            'schema': {
                                'type': 'string'
                            },
                            'required': True
                        }
                        required_parameters.append(parameter)
                    existed_parameters = operations[method].get('parameters', [])
                    operations[method]['parameters'] = existed_parameters + required_parameters
        return node.uri_template

class Docs:
    def on_get(self, req, resp):
        # request_scheme = get_request_scheme(req)
        # http_host = req.env.get('HTTP_HOST')
        # endpoint_root = get_endpoint_root(req)
        spec = APISpec(
            title=f'Evite',
            version="0.1.0",
            openapi_version="3.0.2",
            info=dict(description=f'API Documentaion'),
            plugins=[
                MarshmallowPlugin(),
                FalconPlugin()
            ]
        )
        spec.components.schema('Event', schema=EventSchema)
        spec.components.schema('EventAttendee', schema=EventAttendeeSchema)
        from evite.application import create_application # pylint: disable=C0415,R0401
        for node in get_all_nodes(create_application(), use_blocklist=True):
            spec.path(node=node)
        resp.media = spec.to_dict()