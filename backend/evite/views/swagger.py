import falcon


class SwaggerUI:

    def on_get(self, req, resp):
        resp.content_type = falcon.MEDIA_HTML
        resp.body = '''
            <!-- HTML for static distribution bundle build -->
            <!DOCTYPE html>
            <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>Swagger UI</title>
                <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.28.0/swagger-ui.css" >
                <style>
                  html
                  {
                    box-sizing: border-box;
                    overflow: -moz-scrollbars-vertical;
                    overflow-y: scroll;
                  }

                  *,
                  *:before,
                  *:after
                  {
                    box-sizing: inherit;
                  }

                  body
                  {
                    margin:0;
                    background: #fafafa;
                  }
                </style>
              </head>

              <body>
                <div id="swagger-ui"></div>

                <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.28.0/swagger-ui-bundle.js"> </script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.28.0/swagger-ui-standalone-preset.js"> </script>
                <script>
                window.onload = function() {
                  // Begin Swagger UI call region
                  const ui = SwaggerUIBundle({
                    url: "./docs",
                    oauth2RedirectUrl: `${window.location.protocol}//${window.location.host}${window.location.pathname}oauth2-redirect.html`,
                    validatorUrl: null,
                    dom_id: '#swagger-ui',
                    deepLinking: true,
                    presets: [
                      SwaggerUIBundle.presets.apis,
                      SwaggerUIStandalonePreset
                    ],
                    plugins: [
                      SwaggerUIBundle.plugins.DownloadUrl
                    ],
                    layout: "StandaloneLayout"
                  })
                  ui.initOAuth({
                    clientId: "rns:roc:omnia"
                  })
                  // End Swagger UI call region

                  window.ui = ui
                }
              </script>
              </body>
            </html>
            '''

    def on_get_oauth2(self, req, resp):
        resp.content_type = falcon.MEDIA_HTML
        resp.body = '''
            <!doctype html>
            <html lang="en-US">
            <title>Swagger UI: OAuth2 Redirect</title>
            <body onload="run()">
            </body>
            </html>
            <script>
                'use strict';
                function run () {
                    var oauth2 = window.opener.swaggerUIRedirectOauth2;
                    var sentState = oauth2.state;
                    var redirectUrl = oauth2.redirectUrl;
                    var isValid, qp, arr;

                    if (/code|token|error/.test(window.location.hash)) {
                        qp = window.location.hash.substring(1);
                    } else {
                        qp = location.search.substring(1);
                    }

                    arr = qp.split("&")
                    arr.forEach(function (v,i,_arr) { _arr[i] = '"' + v.replace('=', '":"') + '"';})
                    qp = qp ? JSON.parse('{' + arr.join() + '}',
                            function (key, value) {
                                return key === "" ? value : decodeURIComponent(value)
                            }
                    ) : {}

                    isValid = qp.state === sentState

                    if ((
                      oauth2.auth.schema.get("flow") === "accessCode"||
                      oauth2.auth.schema.get("flow") === "authorizationCode"
                    ) && !oauth2.auth.code) {
                        if (!isValid) {
                            oauth2.errCb({
                                authId: oauth2.auth.name,
                                source: "auth",
                                level: "warning",
                                message: "Authorization may be unsafe, passed state was changed in server Passed state wasn't returned from auth server"
                            });
                        }

                        if (qp.code) {
                            delete oauth2.state;
                            oauth2.auth.code = qp.code;
                            oauth2.callback({auth: oauth2.auth, redirectUrl: redirectUrl});
                        } else {
                            let oauthErrorMsg
                            if (qp.error) {
                                oauthErrorMsg = "["+qp.error+"]: " +
                                    (qp.error_description ? qp.error_description+ ". " : "no accessCode received from the server. ") +
                                    (qp.error_uri ? "More info: "+qp.error_uri : "");
                            }

                            oauth2.errCb({
                                authId: oauth2.auth.name,
                                source: "auth",
                                level: "error",
                                message: oauthErrorMsg || "[Authorization failed]: no accessCode received from the server"
                            });
                        }
                    } else {
                        oauth2.callback({auth: oauth2.auth, token: qp, isValid: isValid, redirectUrl: redirectUrl});
                    }
                    window.close();
                }
            </script>
            '''
