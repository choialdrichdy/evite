import falcon
from falcon.errors import HTTPInternalServerError, HTTPBadRequest
from evite.services.hooks import model_validator
from evite.models.event import EventSchema, EventAttendeeSchema
from evite.services.event import (
    add_events, 
    get_events, 
    add_event_attendees, 
    get_attendees, 
    remove_event_attendee, 
    edit_event, 
    remove_event,
    check_attendee_exists
)

class EventsCollection:
    def on_get(self, req, resp):
        '''
        ---
        summary: Fetch all events
        tags:
          - events
        responses:
          200:
            description: OK
            content:
              application/json:
                schema:
                  type: array
                  items:
                    $ref: '#/components/schemas/Event'
        '''
        resp.media = EventSchema(many=True).dump(get_events(session=req.context['session']))

    @falcon.before(model_validator, EventSchema())
    def on_post(self, req, resp):
        '''
        ---
        summary: Add one event
        tags:
          - events
        requestBody:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Event'
        responses:
          201:
            description: OK
            content:
              application/json:
                schema:
                  type: object
                  $ref: '#/components/schemas/Event'
        '''
        event = req.media
        new_event = add_events(event, session=req.context['session'])
        resp.media = EventSchema().dump(new_event)
        resp.status = falcon.HTTP_201

class EventItem:
    def on_get(self, req, resp, event_id):
        '''
        ---
        summary: Fetch one event
        tags:
          - events
        responses:
          200:
            description: OK
            content:
              application/json:
                schema:
                  type: array
                  items:
                  $ref: '#/components/schemas/Event'
        '''
        resp.media = EventSchema(many=True).dump(get_events(event_id=event_id, session=req.context['session']))

    def on_put(self, req, resp, event_id):
        '''
        ---
        summary: Edit one event
        tags:
          - events
        requestBody:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Event'
        responses:
          200:
            description: OK
            content:
              application/json:
                schema:
                  type: object
                  $ref: '#/components/schemas/Event'
        '''
        event = req.media
        event = edit_event(event_id, event, session=req.context['session'])
        resp.media = EventSchema().dump(event)
        resp.status = falcon.HTTP_200

    def on_delete(self, req, resp, event_id):
        '''
        ---
        summary: Delete one event
        tags:
          - events
        responses:
          204:
            description: No Content
        '''
        remove_event(event_id, session=req.context['session'])
        resp.status = falcon.HTTP_204

class EventAttendeesCollection:
    def on_get(self, req, resp, event_id):
        '''
        ---
        summary: Fetch all event attendees
        tags:
          - eventsattendee
        responses:
          200:
            description: OK
            content:
              application/json:
                schema:
                  type: array
                  items:
                    schema:
                    type: string
        '''
        resp.media = [attendee.email for attendee in get_attendees(event_id, session=req.context['session'])]

    @falcon.before(model_validator, EventAttendeeSchema())
    def on_post(self, req, resp, event_id):
        '''
        ---
        summary: Attend one event
        tags:
          - eventsattendee
        requestBody:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EventAttendee'
        responses:
          201:
            description: OK
            content:
              application/json:
                schema:
                  type: object
                  $ref: '#/components/schemas/EventAttendee'
          400:
            description: BadRequest
            content:
              application/json:
                schema:
                  type: object
                  properties:
                    title:
                      schema:
                      type: string
                    description:
                      schema:
                      type: string
        '''
        event_attendee = req.media
        if check_attendee_exists(event_id, event_attendee, session=req.context['session']):
            raise HTTPBadRequest(description='Email is already in the list!')
        new_attendee = add_event_attendees(event_id, event_attendee, session=req.context['session'])
        resp.media = EventAttendeeSchema().dump(new_attendee)
        resp.status = falcon.HTTP_201

class EventAttendeeItem:
    def on_get(self, req, resp, event_id, email):
        '''
        ---
        summary: Fetch one event attendee
        tags:
          - eventsattendee
        responses:
          200:
            description: OK
            content:
              application/json:
                schema:
                  type: array
                  items:
                    schema:
                    type: string
        '''
        resp.media = [attendee.email for attendee in get_attendees(event_id, email=email, session=req.context['session'])]

    def on_delete(self, req, resp, event_id, email):
        '''
        ---
        summary: Delete one attendee
        tags:
          - eventsattendee
        responses:
          204:
            description: No Content
        '''
        remove_event_attendee(event_id, email, session=req.context['session'])
        resp.status = falcon.HTTP_204