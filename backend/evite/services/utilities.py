import smtplib
import jinja2
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

def render_template(template, **kwargs):
    ''' renders a Jinja template into HTML '''
    templateLoader = jinja2.FileSystemLoader(searchpath="evite/templates/")
    templateEnv = jinja2.Environment(loader=templateLoader, autoescape=True)
    templ = templateEnv.get_template(template)
    return templ.render(**kwargs)

def send_email(event, attendee):
    body = render_template('new_attendee.html.j2', event=event, attendee=attendee)
    # now you can play with your code. Let’s define the SMTP server separately here:
    port = 25
    smtp_server = "localhost"

    # specify the sender’s and receiver’s email addresses
    sender = "from@example.com"
    receiver = "mailtrap@example.com"

    message = MIMEMultipart('alternative')
    message['From'] = sender
    message['Subject'] = 'New attendee!'
    message['To'] = receiver
    message.attach(MIMEText(body, 'html'))
    try:
        #send your message with credentials specified above
        with smtplib.SMTP(smtp_server, port) as server:
            server.sendmail(sender, receiver, message.as_string())
    
        # tell the script to report if your message was sent or which errors need to be fixed 
        print('Sent')
    except smtplib.SMTPException as e:
        print('SMTP error occurred: ' + str(e))