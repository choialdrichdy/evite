from falcon.errors import HTTPInternalServerError, HTTPUnprocessableEntity
import json

def model_validator(req, resp, resource, params, model):
    if model is None:
        raise HTTPInternalServerError(description="Model instance was not passed onto the model validator")
    payload = req.media
    if not isinstance(payload, dict):
        try:
            payload = json.loads(payload)
        except Exception as e:
            raise HTTPUnprocessableEntity(description="Not a valid JSON")
    errors = model.validate(payload)
    if errors:
        raise HTTPUnprocessableEntity(description=errors)