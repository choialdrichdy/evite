from datetime import datetime

from evite.db import get_session
from evite.models.event import Event, EventAttendee
from evite.services.hooks import model_validator
from evite.services.utilities import send_email


def get_events(event_id=None, session=None):
    if session is None:
        session = get_session()
    query = session.query(Event)
    if event_id:
        query.filter(Event.id == event_id)
    return query.all()

def add_events(event, session=None):
    if session is None:
        session = get_session()
    new_event = Event(
        name=event.get('name'),
        location=event.get('location'),
        start_date=datetime.fromisoformat(event.get('start_date')),
        end_date=datetime.fromisoformat(event.get('end_date'))
    )
    session.add(new_event)
    session.commit()
    return new_event

def edit_event(event_id, updated_event, session=None):
    if session is None:
        session = get_session()
    
    event = session.query(Event).filter(Event.id == event_id).scalar()
    event.name = updated_event.get('name')
    event.location = updated_event.get('location')
    event.start_date = datetime.fromisoformat(updated_event.get('start_date'))
    event.end_date = datetime.fromisoformat(updated_event.get('end_date'))
    session.commit()
    return event

def remove_event(event_id, session=None):
    if session is None:
        session = get_session()
    event = session.query(Event).filter(Event.id == event_id).scalar()
    if event:
        session.delete(event)
        session.commit()

def check_attendee_exists(event_id, attendee, session=None):
    if session is None:
        session = get_session()
    attendee = session.query(EventAttendee).filter(EventAttendee.event_id == event_id, EventAttendee.email == attendee.get('email')).scalar()
    if attendee:
        return True
    return False

def get_attendees(event_id, email=None, session=None):
    if session is None:
        session = get_session()
    query = session.query(EventAttendee).filter(EventAttendee.event_id == event_id)
    if email:
        query.filter(EventAttendee.email == email)
    return query.all()

def add_event_attendees(event_id, attendee, session=None):
    if session is None:
        session = get_session()
    new_attendee = EventAttendee(
        event_id=event_id,
        email=attendee.get('email')
    )
    session.add(new_attendee)
    session.commit()
    # Send email here
    event = session.query(Event).filter(Event.id == event_id).scalar()
    send_email(event, new_attendee)
    return new_attendee

def remove_event_attendee(event_id, email, session=None):
    if session is None:
        session = get_session()
    attendee = session.query(EventAttendee).filter(EventAttendee.email == email, EventAttendee.event_id == event_id).scalar()
    if attendee:
        session.delete(attendee)
        session.commit()