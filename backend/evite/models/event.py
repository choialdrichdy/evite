from marshmallow import Schema, fields, INCLUDE, validates_schema, ValidationError, post_load

from sqlalchemy import Column, Integer, UnicodeText, DateTime, ForeignKey
from sqlalchemy.orm import relationship

from evite.models.base import Base


class EventAttendee(Base):
    __tablename__ = "event_attendees"

    event_id = Column(Integer, ForeignKey('events.id'), primary_key=True)
    email = Column(UnicodeText, nullable=False, primary_key=True)


class EventAttendeeSchema(Schema):
    email = fields.Str(required=True)


class Event(Base):
    __tablename__ = 'events'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(UnicodeText, nullable=False)
    start_date = Column(DateTime(timezone=True), nullable=False)
    end_date = Column(DateTime(timezone=True), nullable=False)
    location = Column(UnicodeText, nullable=False)

    attendees = relationship(EventAttendee)


class EventSchema(Schema):
    id = fields.Integer(required=False)
    name = fields.Str(required=True)
    location = fields.Str(required=True)
    start_date = fields.DateTime(required=True)
    end_date = fields.DateTime(required=True)