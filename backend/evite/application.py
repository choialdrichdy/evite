import falcon
from evite.views.docs import Docs
from evite.views.swagger import SwaggerUI
from evite.views.events import EventsCollection, EventItem, EventAttendeesCollection, EventAttendeeItem
from evite.db import SessionManager, Session

def create_application():
    application = falcon.API(middleware=[SessionManager(Session)])
    application.add_route('/', SwaggerUI())
    application.add_route('/docs', Docs())
    application.add_route('/events', EventsCollection())
    application.add_route('/events/{event_id}', EventItem())
    application.add_route('/events/{event_id}/attendees', EventAttendeesCollection())
    application.add_route('/events/{event_id}/attendees/{email}', EventAttendeeItem())
    return application