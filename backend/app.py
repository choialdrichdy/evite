from evite.application import create_application
from datetime import datetime

app = create_application()

if __name__ == '__main__':
    from wsgiref.simple_server import make_server

    with make_server('', 5000, app) as httpd:
        print('Serving on port 5000')

        # Serve until process is killed
        httpd.serve_forever()