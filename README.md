# Evite

Requirements:
- Python 3

How to run the application
1. Clone this repo.
2. Optional: use a virtual environment by running `virtualenv <folder_name>` and `source <folder_name>/bin/activate`
3. Run `cd backend/` and `pip install -r requirements.txt`
4. Run the debug smtp server by `python -m smtpd -n -c DebuggingServer localhost:<port_number>` Note: use `sudo` if you want to use port 25.
5. Overwrite any configuration in `config.py` by exporting environment variables `export XXXX=YYYY`
6. Run `gunicorn --bind 0.0.0.0:5000 app:app` to run the wsgi server
7. Access `localhost:5000`